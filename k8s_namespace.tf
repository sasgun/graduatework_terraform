resource "kubernetes_namespace" "ns" {

  for_each = var.namespaces

  metadata {
    annotations = {
      name = each.key
    }

    labels = {
      mylabel = each.key
    }

    name = each.key
  }
}

resource "kubernetes_namespace" "sonarqube" {

  metadata {
    annotations = {
      name = "sonarqube"
    }

    labels = {
      mylabel = "sonarqube"
    }

    name = "sonarqube"
  }
}

