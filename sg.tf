resource "aws_security_group" "worker_group" {
  name_prefix = "worker_group"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }
  tags = {
    Name = "Worker-SG"
  }
}

resource "aws_security_group" "rds-sg" {
  name   = "RDS-SG"
  vpc_id = module.vpc.vpc_id
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",
    ]
#    cidr_blocks = var.private_subnet_cidrs
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "10.0.0.0/8",
    ]
#    cidr_blocks = var.private_subnet_cidrs
  }

  tags = {
    Name = "RDS-SG"
  }
}

resource "aws_security_group" "rds-sonarqube" {
  name   = "RDS-SONARQUBE"
  vpc_id = module.vpc.vpc_id
  ingress {
    from_port       = 5432
    to_port         = 5432 
    protocol        = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",
    ]
#    cidr_blocks = var.private_subnet_cidrs
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "10.0.0.0/8",
    ]
#    cidr_blocks = var.private_subnet_cidrs
  }

  tags = {
    Name = "RDS-SONARQUBEG"
  }
}

