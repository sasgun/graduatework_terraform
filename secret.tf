data "kubernetes_secret" "gitlab_sa_secret_develop" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account_develop.default_secret_name
    namespace = "develop"
  }

    depends_on = [
    module.vpc,
    kubernetes_namespace.ns
  ]

}

data "kubernetes_secret" "gitlab_sa_secret_production" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account_production.default_secret_name
    namespace = "production"
  }
    depends_on = [
    module.vpc,
    kubernetes_namespace.ns
  ]

}

resource "kubernetes_secret" "docker_registry" {
  for_each = var.namespaces
  metadata {
    name = "imagepullsecret"
    namespace = each.value


  }

  data = {
    ".dockerconfigjson" = "${data.template_file.docker_config_script.rendered}"
  }

  type = "kubernetes.io/dockerconfigjson"

    depends_on = [
    kubernetes_namespace.ns
  ]
}


data "template_file" "docker_config_script" {
  template = "${file("pull-secret.json")}"
  vars = {
    docker-username           = "${var.docker_username}"
    docker-password           = "${var.docker_password}"
    docker-server             = "${var.docker_server}"
    docker-email              = "${var.docker_email}"
    auth                      = base64encode("${var.docker_username}:${var.docker_password}")
  }
}

resource "kubernetes_secret" "rds_password" {

  for_each = var.namespaces

  metadata {
    name      = "rds-password"
    namespace = kubernetes_namespace.ns[each.key].metadata.0.name
    labels = {
      "sensitive" = "true"
      
    }
  }

  data = {
    string = "${random_password.db_password[each.key].result}"
  }

    depends_on = [
    module.vpc,
    kubernetes_namespace.ns
  ]

}

resource "kubernetes_secret" "rds_password_prod" {

  

  metadata {
    name      = "rds-host"
    namespace = "production"
    labels = {
      "sensitive" = "true"
      
    }
  }

  data = {
    string = "${aws_db_instance.graduatework_prod.endpoint}"
  }

    depends_on = [
    module.vpc,
    kubernetes_namespace.ns
  ]

}
resource "kubernetes_secret" "rds_password_dev" {

  

  metadata {
    name      = "rds-host"
    namespace = "develop"
    labels = {
      "sensitive" = "true"
      
    }
  }

  data = {
    string = "${aws_db_instance.graduatework_dev.endpoint}"
  }

    depends_on = [
    module.vpc,
    kubernetes_namespace.ns
  ]

}

resource "kubernetes_secret" "rds_username" {

  for_each = var.namespaces

  metadata {
    name      = "rds-username"
    namespace = kubernetes_namespace.ns[each.key].metadata.0.name
    labels = {
      "sensitive" = "true"
      
    }
  }

  data = {
    string = "${var.db_user}"
  }

    depends_on = [
    module.vpc,
    kubernetes_namespace.ns
  ]

}

resource "kubernetes_secret" "rds_database" {

  for_each = var.namespaces

  metadata {
    name      = "rds-database"
    namespace = kubernetes_namespace.ns[each.key].metadata.0.name
    labels = {
      "sensitive" = "true"
      
    }
  }

  data = {
    string = "${var.db_name}"
  }
    depends_on = [
    module.vpc,
    kubernetes_namespace.ns 
  ]

}

