resource "aws_eks_cluster" "graduate_work" {
  name                      = var.cluster_name
  role_arn                  = "arn:aws:iam::156001095759:role/eks_role" 
  enabled_cluster_log_types = ["api", "audit"]

  vpc_config {
    subnet_ids = var.subnet_ids 
  }

  tags = {
              owner = "aleksandr_dubinin@epam.com"
       }

  tags_all = {
              owner = "aleksandr_dubinin@epam.com"
       }

}


resource "aws_eks_node_group" "workernode" {
  cluster_name    = aws_eks_cluster.graduate_work.name
  node_group_name = "node"
  node_role_arn   = "arn:aws:iam::156001095759:role/EKS_nodegroup_role" 
  subnet_ids      = var.subnet_ids
  instance_types  = [ "t3.medium" ]

  scaling_config {
    desired_size = 2
    max_size     = 3
    min_size     = 1
  }

  update_config {
    max_unavailable = 1
  }


  tags = {
              owner = "aleksandr_dubinin@epam.com"
       }

  tags_all = {
              owner = "aleksandr_dubinin@epam.com"
       }


}


data "aws_eks_cluster" "cluster" {
  name = aws_eks_cluster.graduate_work.name
}

data "aws_eks_cluster_auth" "cluster" {
  name = aws_eks_cluster.graduate_work.name
}



