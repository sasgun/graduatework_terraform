
resource "aws_db_parameter_group" "rds_params" {
  name   = "rds-params"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}



resource "aws_db_instance" "graduatework" {
  identifier             = "graduatework-ad"
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t2.micro"
  username               = var.db_user
  password               = var.db_pass
  db_name                = var.db_name
  allocated_storage      = 10
  storage_type           = "gp2"
  vpc_security_group_ids = [var.sg_id]
  skip_final_snapshot    = true
  identifier_prefix      = null
  multi_az               = false
  storage_encrypted      = false
  snapshot_identifier    = null

  parameter_group_name   = aws_db_parameter_group.rds_params.name

  tags = {
    Name = "DB"
    owner = "aleksandr_dubinin@epam.com"
  }
}

