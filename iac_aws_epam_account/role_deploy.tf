resource "kubernetes_cluster_role" "cluster-deployer" {
  metadata {
    name = "cluster-deployer"
  }

  rule {
    api_groups = ["", "extensions", "apps", "batch", "events", "certmanager.k8s.io", "cert-manager.io", "monitoring.coreos.com"] 
    resources  = ["*"]
    verbs      = ["*"]
  }

}

resource "kubernetes_service_account" "gitlab_service_account_develop" {
  metadata {
    name = "gitlab-service-account-develop"
  }
}

resource "kubernetes_cluster_role_binding" "gitlab_role_bindingi_develop" {
  metadata {
    name = "gitlab_role_binding_develop"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-deployer"
  }

  subject {
    kind = "ServiceAccount"
    name = "gitlab-service-account-develop"
    namespace = "develop"
  }
}

resource "kubernetes_service_account" "gitlab_service_account_production" {
  metadata {
    name = "gitlab-service-account-production"
  }
}

resource "kubernetes_cluster_role_binding" "gitlab_role_binding_production" {
  metadata {
    name = "gitlab_role_binding_production"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-deployer"
  }

  subject {
    kind = "ServiceAccount"
    name = "gitlab-service-account-production"
    namespace = "production"
  }
}

