data "kubernetes_secret" "gitlab_sa_secret_develop" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account_develop.default_secret_name
  }
}

data "kubernetes_secret" "gitlab_sa_secret_production" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account_production.default_secret_name
  }
}

