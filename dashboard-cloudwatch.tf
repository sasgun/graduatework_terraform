data "template_file" "dashboard-template" {
  template = file("dashboard.json")
}

resource "aws_cloudwatch_dashboard" "eks-dashboard" {
  dashboard_name = "graduate-dashboard"
  dashboard_body = data.template_file.dashboard-template.rendered
}

