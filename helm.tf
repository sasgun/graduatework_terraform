#=====================REPO_CHARTS==================================

resource "helm_release" "cert_manager" {
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  name       = "cert-manager"

  create_namespace = true
  namespace        = "certmanager"

  set {
    name  = "installCRDs"
    value = "true"
  }

  depends_on = [
	aws_eks_cluster.graduate_work,
        aws_eks_node_group.workernode
  ]
}

resource "helm_release" "ingress_nginx" {
  name             = "ingress-nginx"
  namespace        = "ingress"
  create_namespace = true
  chart            = "ingress-nginx"
  version          = "4.0.16"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  timeout          = 150 

  values = [
    <<-EOF
    controller:
      service:
        annotations:
          service.beta.kubernetes.io/aws-load-balancer-type: alb
	  service.beta.kubernetes.io/aws-load-balancer-name: "k8s-cluster"
    EOF
  ]


  depends_on = [
    aws_eks_cluster.graduate_work,
    aws_eks_node_group.workernode
  ]
}

resource "helm_release" "sonarqube" {
  name          = "sonarqube"
  namespace     = "sonarqube"
  repository    = "https://SonarSource.github.io/helm-chart-sonarqube"
  chart         = "sonarqube"
  timeout       = 1800
  force_update  = "true"
  recreate_pods = "true"

  values = [
    templatefile("helm/sonarqube/values.yaml", {
      db_host = aws_db_instance.graduatework_sonarqube.endpoint
      username = "sonarqubeuser"
      password = random_password.postgresql_password.result
      database = "sonarqube"
    })
  ]

  depends_on = [
    helm_release.cert_manager,
    helm_release.ingress_nginx,
    kubernetes_namespace.sonarqube
  ]

}

resource "helm_release" "cluster_issuer" {
  name      = "cluster-issuer"
  namespace = "certmanager"

  chart = "helm/cert-issuer"


  depends_on = [
    helm_release.ingress_nginx
  ]
}

resource "helm_release" "cluster_autoscaler" {
  name      = "cluster-autoscaler"
  namespace = "kube-system"
  chart = "helm/cluster-autoscaler"


  depends_on = [
    helm_release.metrics_server
  ]
}

resource "helm_release" "metrics_server" {
  name = "metrics-server"

  repository       = "https://charts.bitnami.com/bitnami"
  chart            = "metrics-server"
  namespace        = "metrics-server"
  version          = "5.11.1"
  create_namespace = true

  set {
    name  = "apiService.create"
    value = "true"
    }
  depends_on = [
    aws_eks_cluster.graduate_work,
    aws_eks_node_group.workernode
  ]

}

resource "helm_release" "cloudwatch" {
  name             = "cloudwatch"
  create_namespace = true
  namespace        = "amazon-cloudwatch"
  chart            = "helm/cloudwatch"

  set {
    name  = "regionName"
    value = var.region
  }
  set {
    name  = "clusterName"
    value = var.cluster_name
  }

  depends_on = [
    helm_release.cluster_issuer
  ]
}

