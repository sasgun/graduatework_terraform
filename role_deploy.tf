resource "kubernetes_cluster_role" "cluster-deployer" {
  metadata {
    name = "cluster-deployer"
  }

  rule {
    api_groups = ["", "extensions", "apps", "batch", "events", "certmanager.k8s.io", "networking.k8s.io", "cert-manager.io", "autoscaling"] 
    resources  = ["*"]
    verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"]
  }

}

resource "kubernetes_service_account" "gitlab_service_account_develop" {
  metadata {
    name = "gitlab-service-account-develop"
    namespace = "develop"
  }
}

resource "kubernetes_cluster_role_binding" "gitlab_role_bindingi_develop" {
  metadata {
    name = "gitlab_role_binding_develop"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-deployer"
  }

  subject {
    kind = "ServiceAccount"
    name = "gitlab-service-account-develop"
    namespace = "develop"
  }
}

resource "kubernetes_service_account" "gitlab_service_account_production" {
  metadata {
    name = "gitlab-service-account-production"
    namespace = "production"
  }
}

resource "kubernetes_cluster_role_binding" "gitlab_role_binding_production" {
  metadata {
    name = "gitlab_role_binding_production"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-deployer"
  }

  subject {
    kind = "ServiceAccount"
    name = "gitlab-service-account-production"
    namespace = "production"
  }
}

#resource "kubernetes_service_account" "cluster-autoscaler" {
 # metadata {
 #   name = "cluster-autoscaler"
 ##   namespace = "kube-system"
   # annotations = {
   #   "eks.amazonaws.com/role-arn" = "arn:aws:iam::${var.ACCOUNT_ID}:policy/eks-cluster-autoscale"
   # }
   # labels = {
   #   "k8s-addon" = "cluster-autoscaler.addons.k8s.io"
   #   "k8s-app" = "cluster-autoscaler"

    #}
  #}
 # automount_service_account_token = true
 # depends_on = [aws_iam_policy.k8s-asg-policy,
 #               ]

#}

